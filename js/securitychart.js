(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.yourModuleBehavior = {
      attach: function (context, settings) {
        var dataplot = drupalSettings.piedata;
        // console.log('Dynamic Data:', dataplot);
        var ctx = document.getElementById('chart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['OK', 'Not Ok'],
                datasets: [{
                    label: 'number of criteria analised',
                    //data: [12, 5],
                    data: dataplot,
                    backgroundColor: [
                        'green',
                        '#f8d9d8'
                    ],
                    borderColor: [
                      'rgba(73, 192, 11, 0.88)',
                      '#f8d9d8'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Number of criteria accomplished or not in Security Review module'
                    }
                }
            },
          });
        },
      };
  })
(jQuery, Drupal, drupalSettings);
  